const mongoose = require('mongoose')
const Schema = mongoose.Schema

const materiaSchema = new Schema({
  codigo: {
    type: String,
    unique: true,
    required: true
  },
  nombre: {
    type: String
  },
  curso: {
    type: String
  },
  horas: {
    type: Number
  }
})

const Materia = mongoose.model('Materia', materiaSchema) 

module.exports = Materia
