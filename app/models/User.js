const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  codigo: {
    type: String,
    required: true
  },
  nombre: {
    type: String
  },
  password: {
    type: String
  }
})
 
module.exports = mongoose.model('User', userSchema)

