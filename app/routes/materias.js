const express = require('express')
const router = express.Router()

const materiaController = require('../controllers/materiaController')

const auth=require('../middlewares/auth')

router.get('/privada', auth, materiaController.privada)

router.get('/', (req, res) => {
  // index
  materiaController.index(req, res)
})

router.get('/privada', (req, res) => {
  // privada
  materiaController.privada(req, res)
})

router.post('/', (req, res) => {
  materiaController.create(req, res)
})

router.get('/:id', (req, res) => {
  // show con el id
  materiaController.show(req, res)
})


router.delete('/:id', (req, res) => {
  materiaController.remove(req, res)
})

router.put('/:id', (req, res) => {
  materiaController.update(req, res)
})


module.exports = router
