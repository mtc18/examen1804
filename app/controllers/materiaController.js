const Materia = require('../models/Materia.js') 
const { ObjectId } = require('mongodb')

const privada = (req, res) => {
  Materia.find((err, materias) => {
    if (err) {
      return res.status(403).json({
        message: 'No tienes permiso'
      })
    }
    return res.json(materias)
  })
}

const index = (req, res) => {
  Materia.find((err, materias) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo la materia'
      })
    }
    return res.json(materias)
  })
}

const show = (req, res) => {
  const id = req.params.id // los parámetros de la ruta (los que van en /:id)
  Materia.findOne({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no válido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener la materia'
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No tenemos esta materia'
      })
    }
    return res.json(materia)
  })
}

const create = (req, res) => {
  const materia = new Materia(req.body)
  materia.save((err, materia) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la materia',
        error: err
      })
    }
    return res.status(201).json(materia)
  })
}

const remove = (req, res) => {
  const id = req.params.id
  Materia.findOneAndDelete({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado la materia'
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No hemos encontrado la materia'
      })
    }
    return res.json(materia)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Materia.findOne({ _id: id }, (err, materia) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la materia',
        error: err
      })
    }
    if (!materia) {
      return res.status(404).json({
        message: 'No hemos encontrado la materia'
      })
    }

    Object.assign(materia, req.body)

    materia.save((err, materia) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la materia'
        })
      }
      if (!materia) {
        return res.status(404).json({
          message: 'No hemos encontrado la materia'
        })
      }
      return res.json(materia)
    })
  })
}

module.exports = {
  index,
  remove,
  update,
  create,
  show,
  privada
}
