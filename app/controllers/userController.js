const User = require('../models/User')
const servicejwt = require('../services/servicejwt')

const login = (req, res) => {
  const nombre = req.params.nombre
  User.findOne({ _nombre: nombre }, (err, user) => {
    if (!ObjectId.isValid(nombre)) {
      return res.status(404).send()
    }
    if (!user) {
      return res.status(404).json({
        message: 'No hemos encontrado EL NOMBRE'
      })
    }
  })

  const contraseña = req.params.contraseña
  User.findOne({ _contraseña: contraseña }, (err, user) => {
    if (!ObjectId.isValid(contraseña)) {
      return res.status(404).send()
    }
    if (!user) {
      return res.status(404).json({
        message: 'No hemos encontrado LA CONTRASEÑA'
      })
    }
  })
}//FIN LOGIN


module.exports = { login }
